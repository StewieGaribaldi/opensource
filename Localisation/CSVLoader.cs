// source: https://www.youtube.com/watch?v=c-dzg4M20wY&t=173s
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;

public class CSVLoader
{
    TextAsset csvFile;
    char lineSeparator = '\n';
    char surround = '"';
    string[] fieldSeparator = {"\",\""};

    public void LoadCSV()
    {
        csvFile = Resources.Load<TextAsset>("localisation");
    }

    public Dictionary<string, string> GetDictionaryValues(string attributeId)
    {
        var dictionary = new Dictionary<string, string>();
        var lines = csvFile.text.Split(lineSeparator);
        var attributeIndex = -1;
        var headers = lines[0].Split(fieldSeparator, StringSplitOptions.None);

        for (var i = 0; i < headers.Length; i++)
        {
            if (headers[i].Contains(attributeId))
            {
                attributeIndex = i;
                break;
            }
        }

        var CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

        for (var i = 1; i < lines.Length; i++)
        {
            var line = lines[i];
            var fields = CSVParser.Split(line);

            for (var f = 0; f < fields.Length; f++)
            {
                fields[f] = fields[f].TrimStart(' ', surround);
                fields[f] = fields[f].TrimEnd(surround);
            }

            if (fields.Length > attributeIndex)
            {
                var key = fields[0];
                if (dictionary.ContainsKey(key))
                {
                    continue;
                }

                var value = fields[attributeIndex];
                dictionary.Add(key, value);
            }
        }

        return dictionary;
    }
}
