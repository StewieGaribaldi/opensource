// source: https://www.youtube.com/watch?v=c-dzg4M20wY&t=173s
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalisationSystem
{
    public enum Language
    {
        English,
        French
    }

    public static Language language = Language.French;

    static Dictionary<string, string> localisedEN;

    static Dictionary<string, string> localisedFR;


    public static bool isInit;

    public static void Init()
    {
        var csvLoader = new CSVLoader();
        csvLoader.LoadCSV();

        localisedEN = csvLoader.GetDictionaryValues("en");
        localisedFR = csvLoader.GetDictionaryValues("fr");

        isInit = true;
    }

    public static string GetLocalisedValue(string key)
    {
        if (!isInit)
        {
            Init();
        }

        var value = key;

        switch (language)
        {
            case Language.English:
                localisedEN.TryGetValue(key, out value);
                break;
            case Language.French:
                localisedFR.TryGetValue(key, out value);
                break;

        }

        return value;
    }
}
