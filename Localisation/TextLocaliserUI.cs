// source: https://www.youtube.com/watch?v=c-dzg4M20wY&t=173s
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextLocaliserUI : MonoBehaviour
{
    TextMeshProUGUI textField;

    public string key;

    void Start()
    {
        textField = GetComponent<TextMeshProUGUI>();
        var value = LocalisationSystem.GetLocalisedValue(key);
        textField.text = value;
    }
}
